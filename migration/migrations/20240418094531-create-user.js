module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Users', {
            id: {
                type: Sequelize.DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            firstName: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true,
                field: 'first_name'
            },
            lastName: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true,
                field: 'last_name'
            },
            username: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true
            },
            email: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true,
                unique: true
            },
            password: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true
            },
            isDelete: {
                type: Sequelize.DataTypes.BOOLEAN,
                default: false,
                field: 'is_delete'
            },
            authorizationAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
                field: 'authorization_at'
            },
            createdAt: {
                type: Sequelize.DataTypes.DATE,
                default: Date.now(),
                field: 'created_at'
            },
            updatedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
                field: 'updated_at'
            }
        })
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Users');
    }
};