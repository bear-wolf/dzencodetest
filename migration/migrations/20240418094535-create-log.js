const {DataTypes} = require("sequelize");
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Logs', {
            id: {
                type: Sequelize.DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            author: {
                type: Sequelize.DataTypes.JSON
            },
            url: {
                type: Sequelize.DataTypes.STRING
            },
            method: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false
            },
            req: {
                type: Sequelize.DataTypes.JSON
            },
            res: {
                type: Sequelize.DataTypes.JSON
            },
            createdAt: {
                type: Sequelize.DataTypes.DATE,
                default: Date.now(),
                allowNull: false,
                field: 'created_at'
            },
            updatedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
                field: 'updated_at'
            }
        })
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Logs');
    }
};