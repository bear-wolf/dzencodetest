module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Likes', {
            id: {
                type: Sequelize.DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            userID: {
                type: Sequelize.DataTypes.INTEGER,
                allowNull: true,
                references: { model: 'Users', key: 'id' },
                field: 'user_id'
            },
            commentID: {
                type: Sequelize.DataTypes.INTEGER,
                allowNull: true,
                field: 'comment_id'
            },
            createdAt: {
                type: Sequelize.DataTypes.DATE,
                default: Date.now(),
                allowNull: false,
                field: 'created_at'
            },
            updatedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
                field: 'updated_at'
            }
        })
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Likes');
    }
};