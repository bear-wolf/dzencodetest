# Instruction for use this test application write only for dZENCode company  

## Descriptions
This repository includes the next information:  

**Position: Junior+** (branch master)
- API endpoints can be find go to address: http://localhost:4000/docs
- Queue implementation can be watch: https://gitlab.com/bear-wolf/dzencodetest/-/commit/9ed482308718c3878436f581366c02344c484553
- Cache implementation can be watch: https://gitlab.com/bear-wolf/dzencodetest/-/commit/042f0439b982e8029b650b5baa1c261398b0edf2
- EventEmitter like extend base version can be watch: https://gitlab.com/bear-wolf/dzencodetest/-/commit/71299277c9b60ee72814afa485793b7a73596e84
- JWT implementation located here:
    + https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/db/user.model.ts?ref_type=heads
    + https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/api/v1/routes/auth/controller.ts?ref_type=heads

**Position: Middle** (branch middle)
- RabbitMQ: https://gitlab.com/bear-wolf/dzencodetest/-/commit/b946c698370eb3228642f47e4c236e3dddd00339
- GraphQL: https://gitlab.com/bear-wolf/dzencodetest/-/tree/middle/src/api/v1/routes/graphql?ref_type=heads

## The correction of mistakes
- Added foreign keys in tables DB, and used CamelCase syntax in fields
> https://gitlab.com/bear-wolf/dzencodetest/-/tree/master/migration/migrations?ref_type=heads

> https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/db/index.ts?ref_type=heads#L24

- Added recursion to commentCtrl
> https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/api/v1/routes/comment/controller.ts?ref_type=heads#L43

- I changed swagger documentation ONLY auth ctrl
> https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/api/v1/routes/auth/index.ts?ref_type=heads

- Regarding the optimization of the signUp function, I added a unique attribute to the user.email database field
> https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/api/v1/routes/auth/controller.ts?ref_type=heads#L53

- Added **error.helper** for parse SQLDataBaseError(hide another info with user in frontEnd)
> https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/api/v1/helpers/error.helper.ts?ref_type=heads

**Security**

- Removed keep tokens with DB  
> https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/db/user.model.ts?ref_type=heads

- Created **jwt.helper** which help work with JWT 
> https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/api/v1/routes/auth/controller.ts?ref_type=heads

> https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/api/v1/helpers/jwt.helper.ts?ref_type=heads

* Now, access and refresh token keep only in some user requests

- Created **bcrypt.helper.ts** for implement **security password generate**
> https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/api/v1/helpers/bcrypt.helper.ts?ref_type=heads
* Now, each token keep the next info:
```json
  id: user.id,
  email: user.email
```

**Also, I added**

- Elastic search
> https://gitlab.com/bear-wolf/dzencodetest/-/tree/master/src/modules/elasticSearch?ref_type=heads  
  https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/modules/elasticSearch/helpers.ts?ref_type=heads  

> Work with extend our models with **BaseModel**  
https://gitlab.com/bear-wolf/dzencodetest/-/blob/master/src/db/base.model.ts?ref_type=heads

- GraphQL
> https://gitlab.com/bear-wolf/dzencodetest/-/tree/master/src/api/v1/routes/graphql?ref_type=heads

## Set environment
**Root direction: ./dzencodetest**

**Disable local services**
> systemctl stop postgresql


**Start DB PostgreSQL**
> ./scripts/local-run.sh

**Prepare docker db**  

Before make migration, our need to change host IP to db

- Inspect IP address

> docker ps | grep '5432'

Find <CONTAINER_ID> with name like 'dzencodetest-db-1'  
Example: docker inspect <CONTAINER_ID>

> docker inspect 5799764b756f  

Find properties: NetworkSettings/Networks/IPAddress like **"IPAddress": "172.23.0.2"**,

Change the file like **migration/config/config.json** property host to real IP address of this docker (dzencodetest-db-1)

> nano migration/config/config.json

Change the file like *.env-local* property PGSQL_DB_HOST to real IP address of this docker (dzencodetest-db-1)  
- PGSQL_DB_HOST=172.23.0.2

**Run data migration**  

We use default db, it's mean not need to create database

> cd ./migration   
> npm run up

**Start API**
> docker build -t dzen-api . && docker run --network=host -p 4000:4000 -it dzen-api

* Note - This terminal not closed;

**Swagger API**  

If above instruction runed OK, API endpoints will be here:

> http://localhost:4000/docs/

**How work with API for example postman**

- All api keep in file apis/dZenCodeTest.postman_collection.json

> Sign-up user -> http://localhost:4000/v1/auth/sign-up  
> Sign-in user -> http://localhost:4000/v1/auth/sign-in  
> Create new comment -> http://localhost:4000/v1/comment/  
> Create like -> http://localhost:4000/v1/like/  
> Get comments -> http://localhost:4000/v1/comment/list?user_id=1  
> Create bookmark -> http://localhost:4000/v1/bookmark/  

* another endpoints in the postman collection

**Schema DB**
- schema.db.mwb