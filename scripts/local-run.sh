#!/usr/bin/env bash

set -e

# Copy local credentials
cp .env.local .env

# Build containers
docker-compose build

# Run containers
docker-compose up -d