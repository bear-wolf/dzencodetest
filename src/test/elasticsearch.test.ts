import 'dotenv/config'
import elasticClient from './../modules/elasticSearch'

async function run() {
    await elasticClient.saveDocument('game-of-thrones', 1, {
        character: 'Ned Stark',
        quote: 'Winter is coming.',
        times: 0
    })

    // await elasticClient.update({
    //     index: 'game-of-thrones',
    //     id: '1',
    //     body: {
    //         character: 'Ned Stark',
    //         quote: 'Winter is coming.',
    //         times: 1
    //     }
    // })

    const document = await elasticClient.Client.get({
        index: 'game-of-thrones',
        id: '1'
    })

    console.log(document)
}

await run().catch((error) => console.log('ERROR:', error))
