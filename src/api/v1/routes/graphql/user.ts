import {User} from "../../../../db";

export const userType = `
  type User {
      id: Int!
      firstName: String
      lastName: String
      username: String
      accessToken: String
      refreshToken: String
      email: String
      password: String
      isDelete: Boolean
      createdAt: String
      updatedAt: String
    }
`;

export const userResolvers = {
    getUsers: async () => {
        return await User.findAll({
            raw: true
        });
    },
};