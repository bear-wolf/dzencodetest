export const queryType = `
    type Query {
        getUsers: [User]
        getComments: [Comment]
        test: String
      }  
`;
