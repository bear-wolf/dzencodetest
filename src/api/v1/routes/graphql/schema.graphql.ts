import {buildSchema} from "graphql";
import {userType} from "./user";
import {commentType} from "./comment";
import {queryType} from "./query";

export default buildSchema(`  
  ${queryType}
  ${userType}
  ${commentType}
`);