// The root provides a resolver function for each API endpoint
import {userResolvers} from "./user";
import {commentResolvers} from "./comment";

export default {
    test: () => Math.random(),
    getUsers: async () => await userResolvers.getUsers(),
    getComments: async () => await commentResolvers.getComments()
}