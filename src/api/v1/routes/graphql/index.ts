import express from "express";
import {graphqlHTTP} from "express-graphql";
import schema from "./schema.graphql";
import root from "./root";
import {createHandler} from "graphql-http/lib/use/express"

const router = express.Router();

router.use('/interface', graphqlHTTP({
    schema,
    rootValue: root,
    graphiql: true
}));

router.use('/', createHandler({
    schema: schema,
    rootValue: root
}))

export default router;



