import {Comment} from "../../../../db";

export const commentType = `
  type Comment {
      id: Int!
      userID: Int
      text: String
      likes: Int
      parent: Int
      createdAt: String!
      updatedAt: String
    }
`;

export const commentResolvers = {
    getComments: async () => {
        return await Comment.findAll({
            raw: true
        });
    }
};