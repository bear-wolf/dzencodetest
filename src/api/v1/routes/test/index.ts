import express from "express";
import {general, getOuterData} from "./controller";
import {isJWTToken} from "../../middleware/core";

const router = express.Router();

/**
 * @openapi
 * '/v1/test/':
 *  get:
 *     tags:
 *     - Test Controller
 *     summary: Test work route
 *     responses:
 *      200:
 *        description: Return string
 */
router.get('/', general);

router.get('/get-outer-data', [isJWTToken], getOuterData);

export default router;



