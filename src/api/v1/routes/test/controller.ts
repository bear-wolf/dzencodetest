import emitterObject from "../../../../boot/emitter";
export const general = (req: any, res: any, next: any) => {
    res
        .status(200)
        .send("Site is working!")
}

export const getOuterData = (req: any, res: any, next: any) => {
    emitterObject.addConnection(res);
    emitterObject.getOuterData();
}


export default {
    general,
    getOuterData
}