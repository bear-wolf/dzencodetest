import express from "express";
import {create} from "./controller";
import {createValidation} from "./validate";
import {isID, rest} from "../../middleware/core";

const router = express.Router();
/**
 * @openapi
 * '/v1/like/list':
 *  get:
 *     tags:
 *     - Like Controller
 *     summary: Get likes as list
 *     responses:
 *      200:
 *        description: Return like Array
 *      400:
 *        description: Bad request
 */
router.get('/list', rest);
/**
 * @openapi
 * '/v1/like/':
 *  post:
 *     tags:
 *     - Like Controller
 *     summary: Create like record
 *     parameters:
 *      - name: user_id
 *        in: body
 *        description: The unique user ID
 *        required: true
 *      - name: comment_id
 *        in: body
 *        description: The unique comment ID
 *        required: true
 *      - name: status
 *        in: body
 *        description: Direction - up / down -> true / false
 *        required: true
 *     responses:
 *      200:
 *        description: Return like JSON
 *      400:
 *        description: Bad request
 */
router.post('/', createValidation, create);
/**
 * @openapi
 * '/v1/like/:ID':
 *  get:
 *     tags:
 *     - Like Controller
 *     summary: Get like by ID
 *     parameters:
 *      - name: Unique ID
 *        in: path
 *        description: The unique like ID
 *        required: true
 *     responses:
 *      200:
 *        description: Return like JSON
 *      400:
 *        description: Bad request
 */
router.get('/:id', isID, rest);
/**
 * @openapi
 * '/v1/like/:ID':
 *  delete:
 *     tags:
 *     - Like Controller
 *     summary: Remove like by ID
 *     parameters:
 *      - name: Unique ID
 *        in: path
 *        description: The unique user ID
 *        required: true
 *     responses:
 *      200:
 *        description: Return empty JSON
 *      400:
 *        description: Bad request
 */
router.delete('/:id', isID, rest);

export default router;



