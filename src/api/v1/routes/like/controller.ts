import {Comment, Like} from "../../../../db";
import 'dotenv/config';
import Queue from 'queue'

const setLikeInComment = async (id: number, status: any) => {
    let comment: any = await Comment.findOne({where: {id}});

    if (!comment) return false;

    comment.likes = Number(comment.likes) || 0;

    const value = status ? comment.likes + 1 : comment.likes - 1

    return await comment.update({
        likes: value,
        updated_at: Date.now()
    });
}

export const create = async (req: any, res: any, next: any) => {
    const {userID, commentID, status} = req.body;
    const queueList = new Queue();

    let like = await Like.findOne({
        where: {
            userID,
            commentID
        }
    });

    queueList.push(() => setLikeInComment(commentID, status));
    queueList.push(() => console.log('Test from queue 1'));
    queueList.push(() => console.log('Test from queue 2'));
    queueList.start(() => {})

    if (like) {
        like = await like.update({
            updated_at: Date.now()
        })
        return res.status(200).json(like);
    }

    try {
        like = await Like.create({
            userID,
            commentID,
            createdAt: Date.now()
        })
    } catch (error) {
        return res.status(400).json(error)
    }

    res
        .status(200)
        .json(like)
}
