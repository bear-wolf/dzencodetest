import joi from "joi";

export const createValidation = async (req: any, res: any, next: any) => {
    const validationSchema = joi.object({
        user_id: joi.number().required().error(() => new Error('user_id is required')),
        comment_id: joi.number().required().error(() => new Error('comment_id is required')),
        status: joi.boolean().required().error(() => new Error('status is required -> true: increase; false: decrease'))
    })
    const validator = validationSchema.validate(req.body, {
        abortEarly: false // false - Check all fields in input data
    });

    if (validator.error) return res.status(400).json({message: validator.error.message});

    next()
}