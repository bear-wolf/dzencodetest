import express from "express";
import {isID, rest} from "../../middleware/core";
import {remove} from "./controller";

const router = express.Router();

/**
 * @openapi
 * '/v1/user/list':
 *  get:
 *     tags:
 *     - User Controller
 *     summary: Get user list
 *     responses:
 *      200:
 *        description: Return access token and refresh_token
 *      404:
 *        description: Not Found
 *        content:
 *         application/json:
 *           schema:
 *             $ref: '404'
 */
router.get('/list', rest);
/**
 * @openapi
 * '/v1/user/:ID':
 *  get:
 *     tags:
 *     - User Controller
 *     summary: Get user by ID
 *     parameters:
 *      - name: ID
 *        in: path
 *        description: The user ID
 *        required: true
 *     responses:
 *      200:
 *        description: Return user
 *      404:
 *        description: Not Found
 */
router.get('/:id', isID, rest);
/**
 * @openapi
 * '/v1/user/:ID':
 *  delete:
 *     tags:
 *     - User Controller
 *     summary: Remove user by ID
 *     parameters:
 *      - name: ID
 *        in: path
 *        description: The user ID
 *        required: true
 *     responses:
 *      200:
 *        description: Return user
 *      404:
 *        description: Not Found
 */
router.delete('/:id', isID, remove);

export default router;



