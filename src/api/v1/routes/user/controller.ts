import {Request, Response} from "express";
import elasticClient from "./../../../../modules/elasticSearch";
import {User} from "../../../../db";

export const getList = async (req: Request, res: Response) => {
    let {body} = await elasticClient.getListDocument('user')

    if (!body)
        return res.json(await User.findAll({}))

    if (!body.hits)
        return res.status(404).json({error: 'Not found'});

    const list = body.map((item: any) => item._source)
    res.json(list)
}

export const remove = async (req: Request, res: Response) => {
    const {id} = req.params

    try {
        await User.destroy({where: {id}});
        await elasticClient.destroyDocument('user', id);
    } catch (error) {
        return res.status(400).json(error)
    }

    res.json({})
}