import express from "express";
import {create, getListAsTree} from "./controller";
import {createValidation} from "./validate";
import {isID, queryHasUserID, isJWTToken, rest} from "../../middleware/core";

const router = express.Router();

/**
 * @openapi
 * '/v1/comment/list':
 *  get:
 *     tags:
 *     - Comment Controller
 *     summary: Get comments as array
 *     responses:
 *      200:
 *        description: Return comments array
 *      400:
 *        description: Bad request
 */
router.get('/list', [isJWTToken, queryHasUserID], getListAsTree);
/**
 * @openapi
 * '/v1/comment/':
 *  post:
 *     tags:
 *     - Comment Controller
 *     summary: Create comment
 *     parameters:
 *      - name: text
 *        in: body
 *        description: The comment text
 *        required: true
 *      - name: parent
 *        in: body
 *        description: The comment parent ID
 *     responses:
 *      200:
 *        description: Return JSON
 *      400:
 *        description: Bad request
 */
router.post('/', [isJWTToken, createValidation], create);
/**
 * @openapi
 * '/v1/comment/:ID':
 *  get:
 *     tags:
 *     - Comment Controller
 *     summary: Get comment by ID
 *     parameters:
 *      - name: ID
 *        in: path
 *        description: The unique comment ID
 *        required: true
 *     responses:
 *      200:
 *        description: Return JSON
 *      400:
 *        description: Bad request
 */
router.get('/:id', isID, rest);
/**
 * @openapi
 * '/v1/comment/:ID':
 *  delete:
 *     tags:
 *     - Comment Controller
 *     summary: Remove comment by ID
 *     parameters:
 *      - name: ID
 *        in: path
 *        description: The unique comment ID
 *        required: true
 *     responses:
 *      200:
 *        description: Return empty JSON
 *      400:
 *        description: Bad request
 */
router.delete('/:id', [isJWTToken, isID], rest);

export default router;



