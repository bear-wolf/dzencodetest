import {Comment} from "../../../../db";
import 'dotenv/config';
import {Op} from 'sequelize';

const getCommentsAsTree = async (userID: number, parentIDs: any[]) => {
    const list: any = await Comment.findAll({
        where: {
            ...(userID && !parentIDs.length && {userID: Number(userID)}),
            ...(!parentIDs.length && {parent: null}),
            ...(parentIDs.length && {parent: {[Op.or]: parentIDs}})
        },
        raw: true
    });

    return list;
}


export const getListAsTree = async (req: any, res: any, next: any) => {
    let commentList = [];
    let comments

    try {
        const {id: userID} = req.userdata;
        let maxCount = 10;
        let ids: any[] = [];

        do {
            maxCount--;
            comments = await getCommentsAsTree(userID, ids)

            ids = comments.map((comment: any) => Number(comment.id));
            commentList.push(...comments);
        } while (ids.length > 0 && maxCount > 0);
    } catch (error) {
        return res.status(400).json(error)
    }

    commentList = commentList.sort((a, b) => !a.parent ? -1 : 1);

    const list: any[] = [];
    const ids: number[] = [];
    const itemCell = (comment: any, _comment: any) => {
        if (comment.id === _comment.parent) {
            ids.push(_comment.id)
            comment.child.push({
                ..._comment,
                child: []
            });
        }
        comment.child.map((item: any) => itemCell(item, _comment))
    }
    let maxProcessing = 1000

    do {
        commentList = commentList.filter((_comment: any) => {
            if (!ids.includes(_comment.id)) {
                ids.push(_comment.id)
                if (!_comment.parent) {
                    list.push({
                        ..._comment,
                        child: []
                    });
                    return;
                }
            }
            list.map(cell => itemCell(cell, _comment))
            return !ids.includes(_comment.id)
        })
        maxProcessing--;
    } while (commentList.length > 0 || maxProcessing > 0)

    return res
        .status(200)
        .json(list)
}

export const create = async (req: any, res: any, next: any) => {
    const {text, parent} = req.body;
    const {id: userID} = req.userdata;

    let comment = await Comment.findOne({
        where: {
            text,
            parent,
            userID
        }
    });

    if (comment) return res.status(400).json({error: 'Comment already exists'});

    try {
        comment = await Comment.create({
            userID,
            text,
            parent
        }, {
            raw: true
        })
    } catch (error) {
        return res.status(400).json(error)
    }

    res
        .status(200)
        .json(comment)
}
