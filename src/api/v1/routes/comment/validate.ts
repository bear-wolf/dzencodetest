import joi from "joi";

export const createValidation = async (req: any, res: any, next: any) => {
    const validationSchema = joi.object({
        text: joi.string().required().error(() => new Error('text is required')),
        parent: joi.number().allow(null, '')
    })
    const validator = validationSchema.validate(req.body, {
        abortEarly: false // false - Check all fields in input data
    });

    if (validator.error) return res.status(400).json({message: validator.error.message});

    next()
}