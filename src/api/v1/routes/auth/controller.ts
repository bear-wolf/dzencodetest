import {User} from '../../../../db/';
import RabbitMQConnection from "../../../../modules/rabbitMQ";
import bcryptHelper from "../../helpers/bcrypt.helper";
import jwtHelper from "../../helpers/jwt.helper";
import errorHelper from "../../helpers/error.helper";

export let signIn = async (req: any, res: any, next: any) => {
    const {email, password} = req.body;
    let user: any = await User.findOne({
        where: {
            email
        },
        attributes: {
            include: ['password']
        }
    });

    if (!user) return res.status(400).json({message: 'Email or password are not correct'});

    let object = user.get({plain: true})
    const isComparePassword = await bcryptHelper.comparePassword(password, object.password)

    if (!isComparePassword) return res.status(404).json({message: 'Something went wrong'});

    const {accessToken, refreshToken} = await jwtHelper.getJWTTokens({
        id: object.id,
        email: object.email
    });

    user.setDataValue('authorizationAt', Date.now());
    await user.save();

    object = {
        ...user.toJSON(),
        accessToken,
        refreshToken
    };
    const mqConnection = new RabbitMQConnection()
    await mqConnection.connect();
    await mqConnection.sendNotification({
        key: "email",
        action: "sign-in",
        body: object
    });

    return res.status(200).json(object);
}

export let signUp = async (req: any, res: any, next: any) => {
    let user: any;
    try {
        user = await User.create({
            ...req.body,
            isDelete: false,
            password: await bcryptHelper.hashPassword(req.body.password),
            createdAt: Date.now()
        })
        user = user.toJSON();
        res.status(200)
           .json(user)
    } catch (error) {
        return res.status(400).json(errorHelper.parseSQLError(error))
    }

    const mqConnection = new RabbitMQConnection()
    await mqConnection.connect();
    await mqConnection.sendNotification({
        key: "email",
        action: "sign-up",
        body: user
    });
}

export let renewRefreshToken = async (req: any, res: any, next: any) => {
    const {id, email} = req.userdata
    const user: any = await User.findOne({
        where: {
            id,
            email
        }
    })

    if (!user) return res.status(404).json({message: 'Not found'});

    const {accessToken, refreshToken} = await jwtHelper.getJWTTokens({id, email});

    return res.status(200).json({
        accessToken,
        refreshToken
    })
}

export default {
    signIn,
    signUp,
    renewRefreshToken
}