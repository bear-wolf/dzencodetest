import express from 'express'
import {renewRefreshToken, signIn, signUp} from "./controller";
import {renewRefreshTokenValidation, signInValidation, signUpValidation} from "./validate";

const router = express.Router();

/**
 * @openapi
 * '/v1/auth/sign-in':
 *  post:
 *     tags:
 *     - Auth Controller
 *     summary: Sign in
 *     parameters:
 *      - name: email
 *        description: The email of the user
 *      - name: password
 *        description: The password of the user
 *     requestBody:
 *         content:
 *           application/json:
 *             schema:
 *                properties:
 *                  email:
 *                    type: string
 *                  password:
 *                    type: string
 *     responses:
 *      200:
 *        description: JSON object
 *     400:
 *        description: Bad request
 *        content:
 *         application/json:
 *           schema:
 *             properties:
 *               error:
 *                 type: string
 */
router.post('/sign-in', signInValidation, signIn);
/**
 * @openapi
 * '/v1/auth/sign-up':
 *  post:
 *     tags:
 *     - Auth Controller
 *     summary: Sign up
 *     parameters:
 *      - name: email
 *        in: path
 *        description: The unique email of the user
 *        required: true
 *      - name: password
 *        in: path
 *        description: The password of the user
 *        required: true
 *      - name: username
 *        description: The username of the user
 *      - name: firstName
 *        description: The first name of the user
 *      - name: lastName
 *        description: The last name of the user
 *     requestBody:
 *       description: Just object
 *       content:
 *         application/json:
 *           schema:
 *             properties:
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *               username:
 *                 type: string
 *               firstName:
 *                 type: string
 *               lastName:
 *                 type: string
 *     responses:
 *      200:
 *        description: OK
 *        content:
 *         application/json:
 *           schema:
 *             properties:
 *               id:
 *                 type: integer
 *               firstName:
 *                 type: string
 *               lastName:
 *                 type: string
 *               username:
 *                 type: string
 *               accessToken:
 *                 type: string
 *               refreshToken:
 *                 type: string
 *               email:
 *                 type: string
 *               isDelete:
 *                 type: string
 *               authorizationAt:
 *                 type: string
 *               createdAt:
 *                 type: string
 *               updatedAt:
 *                 type: string
 *      400:
 *        description: Bad request
 */
router.post('/sign-up', signUpValidation, signUp);
/**
 * @openapi
 * '/v1/auth/renew_refresh_token':
 *  post:
 *     tags:
 *     - Auth Controller
 *     summary: Renew refresh token
 *     parameters:
 *      - name: refreshToken
 *        in: path
 *        description: The refresh token of the user
 *        required: true
 *     requestBody:
 *       description: Just object
 *       content:
 *         application/json:
 *           schema:
 *             properties:
 *               refreshToken:
 *                 type: string
 *     responses:
 *      200:
 *        description: Return access token and refresh_token
 *        content:
 *         application/json:
 *           schema:
 *             properties:
 *               accessToken:
 *                 type: string
 *               refreshToken:
 *                 type: string
 *      400:
 *        description: Bad request
 *        content:
 *         application/json:
 *           schema:
 *             properties:
 *               error:
 *                 type: string
 *      404:
 *        description: Not Found
 *        content:
 *         application/json:
 *           schema:
 *             properties:
 *               message:
 *                 type: string
 */
router.post('/renew_refresh_token', renewRefreshTokenValidation, renewRefreshToken);

export default router;
