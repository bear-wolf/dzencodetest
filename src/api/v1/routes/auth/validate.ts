import joi from "joi";
import {User} from "../../../../db";
import jwt from "jsonwebtoken";
import jwtHelper from "../../helpers/jwt.helper";

export const signInValidation = async (req: any, res: any, next: any) => {
    const validationSchema = joi.object({
        email: joi.string().required().error(() => new Error('email is required')),
        password: joi.string().required().error(() => new Error('password is required'))
    });
    const validator = validationSchema.validate(req.body, {abortEarly: true});

    if (validator.error) return res.status(400).json({message: validator.error.message});

    next()
}

export const signUpValidation = async (req: any, res: any, next: any) => {
    const validationSchema = joi.object({
        email: joi.string().required().error(() => new Error('Email is required')),
        password: joi.string().required().error(() => new Error('Password is required')),
        username: joi.string().allow(null, ''),
        firstName: joi.string().allow(null, ''),
        lastName: joi.string().allow(null, '')
    })
    const validator = validationSchema.validate(req.body, {abortEarly: true});

    if (validator.error) return res.status(400).json({message: validator.error.message});

    next()
}

export const renewRefreshTokenValidation = async (req: any, res: any, next: any) => {
    const validationSchema = joi.object({
        refreshToken: joi.string().required().error(() => new Error('refreshToken is required'))
    });
    const validator: any = validationSchema.validate(req.body, {abortEarly: true});

    if (validator.error) return res.status(400).json({message: validator.error.message});

    if (jwtHelper.refreshTokenIsExpire(req.body.refreshToken))
        return res.status(401).json({message: 'Refresh token is expired.'});

    const tokenData: any = jwt.decode(req.body.refreshToken, {})
    const user: any = await User.findOne({
        where: {
            id: tokenData.id,
            email: tokenData.email
        },
        raw: true
    })

    if (!user) return res.status(404).json({message: 'User not found'});

    req.userdata = user
    next()
}
