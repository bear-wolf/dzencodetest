import joi from "joi";

export const createValidation = async (req: any, res: any, next: any) => {
    const validationSchema = joi.object({
        userID: joi.number().required().error(() => new Error('userID is required')),
        commentID: joi.number().required().error(() => new Error('commentID is required')),
        status: joi.boolean().required().error(() => new Error('status is required -> true: save; false: remove'))
    })
    const validator = validationSchema.validate(req.body, {
        abortEarly: false // false - Check all fields in input data
    });

    if (validator.error) return res.status(400).json({message: validator.error.message});

    next()
}