import {BookMark} from "../../../../db";
import 'dotenv/config';

export const getList = async (req: any, res: any, next: any) => {
    let bookmarks

    try {
        bookmarks = await BookMark.findAll({
            where: {
                userID: Number(req.query.userID)
            }
        });
    } catch (error) {
        return res.status(400).json(error)
    }

    res
        .status(200)
        .json(bookmarks)
}

export const create = async (req: any, res: any, next: any) => {
    let bookmark;
    const {userID, commentID, status} = req.body;

    bookmark = await BookMark.findOne({
        where: {
            userID,
            commentID
        }
    });

    if (bookmark) {
        if (status) return res.status(400).json({error: 'BookMark already exists', bookmark})

        await bookmark.destroy()
        return res
            .status(200)
            .json({})
    }

    try {
        bookmark = await BookMark.create({
            userID,
            commentID
        })
    } catch (error) {
        return res.status(400).json(error)
    }

    res
        .status(200)
        .json(bookmark)
}
