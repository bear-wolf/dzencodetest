import express from "express";
import {create, getList} from "./controller";
import {createValidation} from "./validate";
import {isID, queryHasUserID, rest} from "../../middleware/core";

const router = express.Router();
/**
 * @openapi
 * '/v1/bookmark/list':
 *  get:
 *     tags:
 *     - Bookmark Controller
 *     summary: Get bookmarks as array
 *     responses:
 *      200:
 *        description: Return array
 *      400:
 *        description: Bad request
 */
router.get('/list', queryHasUserID, getList);
/**
 * @openapi
 * '/v1/bookmark/':
 *  post:
 *     tags:
 *     - Bookmark Controller
 *     summary: Create bookmark
 *     parameters:
 *      - name: user_id
 *        in: body
 *        description: The unique user ID
 *        required: true
 *      - name: comment_id
 *        in: body
 *        description: The unique comment ID
 *        required: true
 *      - name: status
 *        in: body
 *        description: Direction save / remove -> true / false
 *     responses:
 *      200:
 *        description: Return JSON
 *      400:
 *        description: Bad request
 */
router.post('/', createValidation, create);
/**
 * @openapi
 * '/v1/bookmark/:ID':
 *  get:
 *     tags:
 *     - Bookmark Controller
 *     summary: Get bookmark by ID
 *     parameters:
 *      - name: ID
 *        in: path
 *        description: The unique bookmark ID
 *        required: true
 *     responses:
 *      200:
 *        description: Return JSON
 *      400:
 *        description: Bad request
 */
router.get('/:id', isID, rest);
/**
 * @openapi
 * '/v1/bookmark/:ID':
 *  delete:
 *     tags:
 *     - Bookmark Controller
 *     summary: Remove bookmark by ID
 *     parameters:
 *      - name: ID
 *        in: path
 *        description: The unique bookmark ID
 *        required: true
 *     responses:
 *      200:
 *        description: Return empty JSON
 *      400:
 *        description: Bad request
 */
router.delete('/:id', isID, rest);

export default router;



