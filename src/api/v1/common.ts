import express from 'express'
import allModules from './include-modules'
import testRoutes from './routes/test/index'
import userRoutes from './routes/user/index'
import commentRoutes from './routes/comment/index'
import likeRoutes from './routes/like/index'
import bookmarkRoutes from './routes/bookmark/index'
import authRoutes from "./routes/auth";
import {isJWTToken} from "./middleware/core";
import emitterObject from "../../boot/emitter";
import graphqlRoute from "./routes/graphql/";
import moment from "moment";

const router = express.Router();

allModules();
//Add log any request in DB with eventEmitter
router.use((req: any, res: any, next: any) => {
    req.uniqueID = moment().unix()
    emitterObject.emit('save_req', {req});
    next();
});
router.use('/test', testRoutes);
router.use('/auth', authRoutes);
router.use('/graphql', graphqlRoute);
router.use('/user', (req: any, res: any, next: any)=> { req.entity = 'user'; next() }, userRoutes);
router.use('/comment', [isJWTToken, (req: any, res: any, next: any)=> { req.entity = 'comment'; next() }], commentRoutes);
router.use('/bookmark', (req: any, res: any, next: any)=> { req.entity = 'bookmark'; next() }, bookmarkRoutes);
router.use('/like', [isJWTToken, (req: any, res: any, next: any)=> { req.entity = 'like'; next() }], likeRoutes);

export default router;
