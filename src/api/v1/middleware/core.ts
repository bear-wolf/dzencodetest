import joi from "joi";
import restHelper from "../helpers/rest.helper";
import {BookMark, Comment, Like, User} from "../../../db";
import emitterObject from "../../../boot/emitter";
import NodeCache from "node-cache";
import jwt from "jsonwebtoken";
import jwtHelper from "../helpers/jwt.helper";

const cache = new NodeCache({
    stdTTL: 100
});

export const isID = (req: any, res: any, next: any) => {
    const validationSchema = joi.object({
        id: joi.number().required().error(() => new Error('ID is required')),
    })
    const validator = validationSchema.validate(req.params, {abortEarly: true});

    if (validator.error) return res.status(400).json({message: validator.error.message});

    next()
}

export const isJWTToken = async (req: any, res: any, next: any) => {
    const {authorization: accessToken} = req.headers;

    if (accessToken) {
        try {
            if (jwtHelper.accessTokenIsExpire(accessToken))
                return res.status(401).json({message: 'Authorization token header is expired.'});

            let user: any = cache.get(accessToken);
            let userdata: any

            if (user) {
                userdata = new User(JSON.parse(user))
            } else {
                const tokenData: any = jwt.decode(accessToken, {})
                userdata = await User.findOne({
                    where: {
                        id: tokenData.id,
                        email: tokenData.email
                    }
                });
            }

            !user && cache.set(accessToken, JSON.stringify(userdata), 60000); // 1m

            if (userdata) {
                req.userdata = userdata.toJSON();
                emitterObject.emit('save_req', {req, isUpdate: true});
                return next();
            }
        } catch (error) {
            return res.status(500).json({error, message: 'Something wrong.'});
        }
        return res.status(401).json({message: 'Authorization token it\'s not correct.'});
    }

    return res.status(401).json({message: 'Authorization token header is required.'});
};

export const isUUID = (req: any, res: any, next: any) => {
    const validationSchema = joi.object({
        uuid: joi.string().required().error(() => new Error('UUID is required')),
    })
    const validator = validationSchema.validate({
        uuid: req.query.uuid,
    }, {abortEarly: true});

    if (validator.error) return res.status(400).json({message: validator.error.message});

    next()
}

export const queryHasUserID = async (req: any, res: any, next: any) => {
    const validationSchema = joi.object({
        userID: joi.string().required().error(() => new Error('userID is required')),
    })
    const validator = validationSchema.validate(req.query, {abortEarly: true});

    if (validator.error) return res.status(400).json({message: validator.error.message});

    next()
}

// UNIVERSAL REST, NOT MIDDLEWARE
export const rest = async (req: any, res: any, next: any) => {
    if (!req.entity) return res.status(404).send("No entity found as middleware.");

    const object: any = {
        user: async (req: any, res: any, next: any) => await restHelper.rest(req, res, next, User),
        comment: async (req: any, res: any, next: any) => await restHelper.rest(req, res, next, Comment),
        like: async (req: any, res: any, next: any) => await restHelper.rest(req, res, next, Like),
        bookmark: async (req: any, res: any, next: any) => await restHelper.rest(req, res, next, BookMark)
    }

    try {
        await object[req.entity](req, res, next);
    } catch (error: any) {
        return res.status(400).json({message: error.message});
    }

    return res.status(200).json({});
}


export default {
    isID,
    rest
}