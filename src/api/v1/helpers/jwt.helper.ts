import jwt, {Algorithm} from "jsonwebtoken";
import moment from "moment";
import 'dotenv/config';

export const getJWTTokens = async (userData: object) => {
    const publicKey: string = process.env.SECRET || '';
    const algorithm: Algorithm = 'HS256';
    const accessToken: string = jwt.sign({
        ...userData,
        expired: moment().add(process.env.ACCESS_TOKEN_EXPIRED_MINUTES, 'minutes').unix()
    }, publicKey, {
        algorithm
    });
    const refreshToken: string = jwt.sign({
        ...userData,
        expired: moment().add(process.env.REFRESH_TOKEN_EXPIRED_MINUTES, 'minutes').unix()
    }, publicKey, {
        algorithm
    });

    return {
        accessToken,
        refreshToken
    };
}

export const accessTokenIsExpire = (accessToken: string) => {
    const data: any = jwt.decode(accessToken, {})
    return data.expired < moment().unix()
}

export const refreshTokenIsExpire = (refreshToken: string) => {
    const data: any = jwt.decode(refreshToken, {})
    return data.expired < moment().unix()
}

export default {
    getJWTTokens,
    accessTokenIsExpire,
    refreshTokenIsExpire
}