import {NextFunction, Request, Response} from "express";
import elasticClient from "../../../modules/elasticSearch";

export const getList = async (req: Request, res: Response, next: any, db: any) => {
    let body = await elasticClient.getListDocument(db.type)

    res.json(body || await db.findAll({raw: true}))
}

export const getByID = async (req: any, res: any, next: any, db: any, id: any) => {
    let body = await elasticClient.searchDocument(db.type, id)

    res.json(body || await db.findOne({where: {id}, raw: true}))
}

export const remove = async (db: any, id: any) => {
    await db.destroy({where: {id}});
    await elasticClient.destroyDocument(db.type, id);
}

export const rest = async (req: Request, res: Response, next: NextFunction, db: any) => {
    const query: any = req.url.split('?') || '';
    const subAction: string = query[0].split('/')[1] || '';
    const ID: any = req.params.id || null;
    let json = {}

    try {
        if (subAction === 'list' && req.method === 'GET') return await getList(req, res, next, db)
        if (ID && req.method === 'GET') return await getByID(req, res, next, db, ID);
        if (ID && req.method === 'DELETE') await remove(db, ID);
    } catch (error) {
        return res
            .writeHead(400, {'Content-Type': 'application/json'})
            .json(error)
    }
    return res.json(json).end()
}

export default {
    rest
}