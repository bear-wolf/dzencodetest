export const parseSQLError = (error: any) => {
    if (error.name) {
        return {
            error: error.name,
            fields: error.fields,
            message: `This fields is not correct`
        }
    }
    return {}
}

export default {
    parseSQLError
}