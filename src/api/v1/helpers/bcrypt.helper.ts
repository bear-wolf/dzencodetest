import bcrypt from 'bcrypt'
import 'dotenv/config';

export const hashPassword = async (password: string) => {
    const saltRounds = 10;
    const secret = process.env.SECRET;

    return await new Promise((resolve, reject) => {
        bcrypt.hash(`${password}${secret}`, saltRounds, (err, hash) => {
            err && reject(err) || resolve(hash)
        });
    })
}

export const comparePassword = async (inputPassword: string, storePassword: string) => {
   return await new Promise((resolve, reject) => {
       bcrypt.compare(inputPassword, storePassword, (err, result) => {
           err && reject(err) || resolve({})
       });
   })
}

export default {
    hashPassword,
    comparePassword
}