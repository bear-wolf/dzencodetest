import 'dotenv/config'
import {Client} from '@elastic/elasticsearch'

it("ElasticSearch -> Write/Read", async () => {
    const elasticClient: any = new Client({
        nodes: process.env.ELASTICSEARCH_NODE,
        auth: {
            username: process.env.ELASTIC_USERNAME || '',
            password: process.env.ELASTIC_PASSWORD || ''
        },
    });

    async function run() {
        await elasticClient.index({
            index: 'game-111',
            body: {
                character: 'Ned Stark',
                quote: 'Winter is coming.',
                times: 0
            },
            id: '1'
        })

        // await elasticClient.update({
        //     index: 'game-of-thrones',
        //     id: '1',
        //     body: {
        //         lang: 'painless',
        //         source: 'ctx._source.times++'
        //         // you can also use parameters
        //         // source: 'ctx._source.times += params.count',
        //         // params: { count: 1 }
        //     }
        // })

        // const document = await elasticClient.get({
        //     index: 'game-of-thrones',
        //     id: '1'
        // })

        //console.log(document)
    }

    await run().catch(console.log)

    expect(true).toBe(true);
})