import { Client as ES } from '@elastic/elasticsearch'
import 'dotenv/config';

const elasticClient: any = new ES({
    // cloud: {
    //     id: process.env.ELASTIC_CLOUD_ID,
    // },
    node: process.env.ELASTICSEARCH_NODE || '',
    auth: {
        username: process.env.ELASTIC_USERNAME || '',
        password: process.env.ELASTIC_PASSWORD || '',
    },
});

export default elasticClient;