import Client from './connection'
import {destroyDocument, getListDocument, saveDocument, searchDocument} from "./helpers";

export default {
    Client,
    saveDocument,
    searchDocument,
    getListDocument,
    destroyDocument
}