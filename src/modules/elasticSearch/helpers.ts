import Client from "./connection";

export const saveDocument = async (index: string, id: number, body: object) => {
    const json: any = {index, id, body};
    return Client.index(json)
}

export const destroyDocument = async (index: string, id: any) => {
    return await Client.deleteByQuery({
        index,
        body: {
            query: {
                match: {id}
            }
        }
    });
}

export const getListDocument = async (index: string) => {
    const {body} = await Client.search({
        index,
        body: {
            query: {
                match_all: {}
            }
        }
    })
    return getPlainDocument(body)
}

export const searchDocument = async (index: string, id: number) => {
    const {body} = await Client.search({
        index,
        body: {
            query: {
                match: {id}
            }
        }
    });
    const list: any[] = getPlainDocument(body);
    return list.length ? list[0] : null
}

export const getPlainDocument = (list: any) => {
    return list.hits ? (list.hits.hits || []).map((item: any) => item._source) : null
}

export default {
    saveDocument,
    searchDocument,
    destroyDocument,
    getListDocument,
    getPlainDocument
}