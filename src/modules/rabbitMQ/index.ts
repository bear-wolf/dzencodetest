import client, {Channel, Connection} from "amqplib";
import 'dotenv/config'
import * as process from "process";
import {HandlerCB, iRabbitNotification} from "./notification.type";

class RabbitMQConnection {
    connection!: Connection;
    channel!: Channel;
    public connected!: Boolean;
    public rmqUser: string | undefined;
    public rmqPass: string | undefined;
    public rmqHost: string | undefined;
    public NOTIFICATION_QUEUE: string = '';

    get connectionString() {
        this.rmqUser = process.env.RABBITMQ_DEFAULT_USER;
        this.rmqPass = process.env.RABBITMQ_DEFAULT_PASS;
        this.rmqHost =  process.env.RABBITMQ_URL;
        this.NOTIFICATION_QUEUE = process.env.NOTIFICATION_QUEUE || '';

        return `amqp://${this.rmqUser}:${this.rmqPass}@${this.rmqHost}:5672`
    }

    async connect() {
        if (this.connected && this.channel) return;

        try {
            this.connection = await client.connect(this.connectionString);
            this.channel = await this.connection.createChannel();
            this.connected = true;
        } catch (error) {
            console.error(`Connection string`, this.connectionString);
            console.error(`Not connected to MQ Server`, error);
            this.connected = false;
        }
    }

    async sendToQueue(queue: string, message: any) {
        try {
            if (!this.channel) await this.connect()

            this.channel.sendToQueue(queue, Buffer.from(JSON.stringify(message)));
        } catch (error) {
            console.error(error);
            throw error;
        }
        return true;
    }

    async sendNotification(notification: iRabbitNotification) {
        return await this.sendToQueue(this.NOTIFICATION_QUEUE, notification);
    };

    async consume(handleIncomingNotification: HandlerCB) {
        await this.channel.assertQueue(this.NOTIFICATION_QUEUE, {
            durable: true
        });

        this.channel.consume(
            this.NOTIFICATION_QUEUE,
            (msg) => {
                {
                    if (!msg) return console.error(`Invalid incoming message`);

                    handleIncomingNotification(msg?.content?.toString());
                    this.channel.ack(msg);
                }
            },
            {
                noAck: false
            }
        );
        return true;
    }
}

export default RabbitMQConnection