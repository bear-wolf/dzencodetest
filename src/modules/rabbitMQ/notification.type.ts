export type iRabbitNotification = {
    key: string;
    action?: string;
    description?: string;
    body?: {};
};

export type HandlerCB = (msg: string) => any;