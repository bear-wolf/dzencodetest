import {iRabbitNotification} from "../rabbitMQ/notification.type";

export default interface iEmail {
    message: iRabbitNotification
    getInformation: () => {} // for testing this module
    checkAPI: () => {}
    sendEmail: () => {}
    confirmEmail: () => {}
}