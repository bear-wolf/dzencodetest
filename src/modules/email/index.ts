import RabbitMQConnection from "../rabbitMQ";
import {sendEmail, signIn} from "./email.controller";

const handleIncomingNotification = (msg: any) => {
    try {
        const message: any = JSON.parse(msg)
        if (message.key !== "email") return

        switch (message.action) {
            case 'sign-up': return sendEmail(message)
            case 'sign-in': return signIn(message)
        }
        // Implement your own notification flow
    } catch (error) {
        console.error(`Error While Parsing the message`);
    }
};

export default async (props?: any) => {
    console.log('- email module;')
    const listen = async () => {
        const mqConnection = new RabbitMQConnection();
        await mqConnection.connect();

        return await mqConnection.consume(handleIncomingNotification);
    };

    await listen();
}