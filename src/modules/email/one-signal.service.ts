import * as OneSignal from '@onesignal/node-onesignal';
import 'dotenv/config'
import https from 'https';

const configuration = OneSignal.createConfiguration({
    userKey: process.env.ONESIGNAL_APP_ID,
    appKey: process.env.ONESIGNAL_API_KEY,
});

const client = new OneSignal.DefaultApi(configuration);

var sendNotification = async (data: any) => {
    var headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj"
    };

    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };


    await https.request(options, function (res: any) {
        res.on('data', function (data: any) {
            console.log("Response:");
            console.log(JSON.parse(data));
        });
    });
};

const sendEmail = async (email: any, template: any) => {
    var message = {
        app_id: process.env.ONESIGNAL_APP_ID,
        "include_player_ids": ["de8d6cc3-b9c7-478e-a39b-713a26c70ca2"],
        "email_subject": "Welcome to Cat Facts!",
        "email_body": "<html><head>Welcome to Cat Facts</head><body><h1>Welcome to Cat Facts<h1><h4>Learn more about everyone's favorite furry companions!</h4><hr/><p>Hi Nick,</p><p>Thanks for subscribing to Cat Facts! We can't wait to surprise you with funny details about your favorite animal.</p><h5>Today's Cat Fact (March 27)</h5><p>In tigers and tabbies, the middle of the tongue is covered in backward-pointing spines, used for breaking off and gripping meat.</p><a href='https://catfac.ts/welcome'>Show me more Cat Facts</a><hr/><p><small>(c) 2018 Cat Facts, inc</small></p><p><small><a href='[unsubscribe_url]'>Unsubscribe</a></small></p></body></html>"
    };
    await sendNotification(message);
}

export default {
    sendEmail
}