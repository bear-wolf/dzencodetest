import { Model, Sequelize, DataTypes } from 'sequelize';
import BaseModel from "./base.model";

class LogModel extends BaseModel {
    static type: string = 'log';

    public id?: number;
    public url?: string;
    public author?: object;
    public method?: string;
    public req?: object;
    public res?: object;
    public createdAt?: string;
    public updatedAt?: string;
}

export default LogModel

export const likeAttributes= {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    author: {
        type: DataTypes.JSON
    },
    url: {
        type: DataTypes.STRING,
        allowNull: false
    },
    method: {
        type: DataTypes.STRING,
        allowNull: false
    },
    req: {
        type: DataTypes.JSON,
        allowNull: false
    },
    res: {
        type: DataTypes.JSON
    },
    createdAt: {
        type: DataTypes.DATE,
        default: Date.now(),
        field: 'created_at'
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
        field: 'updated_at'
    }
}

export const LogSchema = (sequelize: Sequelize) => {
    LogModel.init(likeAttributes, {
        sequelize,
        tableName: 'Logs',
        timestamps: false,
        hooks: {
            beforeCreate: async (instance, options) => {
                instance.entityName = 'log';
                instance.setDataValue('createdAt', Date.now());
                return Promise.resolve();
            },
            beforeUpdate: async (instance, options) => {
                instance.entityName = 'log';
                instance.setDataValue('updatedAt', Date.now());
                return Promise.resolve();
            },
            beforeDestroy: async (instance, options) => {
                instance.entityName = 'log';
                return Promise.resolve();
            }
        }
    });
    LogModel.sync();
    return LogModel;
}
