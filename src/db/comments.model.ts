import { Model, Sequelize, DataTypes } from 'sequelize';
import BaseModel from "./base.model";

class CommentModel extends BaseModel {
    static type: string = 'comment';

    public id?: number;
    public userID?: number;
    public parent?: number;
    public text?: string;
    public likes?: number;
    public createdAt?: string;
    public updatedAt?: string;
}

export default CommentModel

export const commentAttributes= {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    userID: {
        type: DataTypes.BIGINT,
        allowNull: false,
        field: 'user_id'
    },
    parent: {
        type: DataTypes.BIGINT,
        allowNull: true
    },
    text: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    likes: {
        type: DataTypes.SMALLINT,
        defaultValue: 0
    },
    createdAt: {
        type: DataTypes.DATE,
        default: Date.now(),
        field: 'created_at'
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
        field: 'updated_at'
    }
}

export const CommentSchema = (sequelize: Sequelize) => {
    CommentModel.init(commentAttributes, {
        sequelize,
        tableName: 'Comments',
        timestamps: false,
        hooks: {
            beforeCreate: async (instance, options) => {
                instance.entityName = 'comment';
                instance.setDataValue('createdAt', Date.now());
                return Promise.resolve();
            },
            beforeUpdate: async (instance, options) => {
                instance.entityName = 'comment';
                instance.setDataValue('updatedAt', Date.now());
                return Promise.resolve();
            },
            beforeDestroy: async (instance, options) => {
                instance.entityName = 'comment';
                return Promise.resolve();
            }
        }
    });
    CommentModel.sync();
    return CommentModel;
}
