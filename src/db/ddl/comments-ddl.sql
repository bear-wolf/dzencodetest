-- comments

DROP TABLE IF EXISTS comments;

CREATE TABLE comments (
  id SERIAL PRIMARY KEY,
  userID SERIAL NOT NULL,
  text TEXT NOT NULL,
  likes SMALLINT DEFAULT 0,
  parent SERIAL DEFAULT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NULL

  FOREIGN KEY (userID) REFERENCES users (id) ON DELETE CASCADE
);

CREATE UNIQUE INDEX comments_id_unique_idx ON comments(id);