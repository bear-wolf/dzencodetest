import {Sequelize} from 'sequelize';
import sequelize from '../modules/connection_postgresql';
import "dotenv";
import {UserSchema} from "./user.model";
import {CommentSchema} from "./comments.model";
import {LikeSchema} from "./likes.model";
import {BookMarksSchema} from "./bookmarks.model";
import {LogSchema} from "./log.model";
import {BaseSchema} from "./base.model";

const db: any = {};

export const Base = BaseSchema(sequelize);
export const User = UserSchema(sequelize);
export const Comment = CommentSchema(sequelize);
export const Like = LikeSchema(sequelize);
export const BookMark = BookMarksSchema(sequelize);
export const Log = LogSchema(sequelize);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

//Association
Comment.belongsTo(User, {foreignKey: 'user_id', targetKey: 'id'});
Like.belongsTo(User, {foreignKey: 'user_id', targetKey: 'id'});
BookMark.belongsTo(User, {foreignKey: 'user_id', targetKey: 'id'});

db.Base = Base;
db.User = User;
db.Comment = Comment;
db.BookMark = BookMark;
db.Like = Like;
db.Log = Log;

export default db;
