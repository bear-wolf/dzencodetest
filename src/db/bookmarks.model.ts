import { Model, Sequelize, DataTypes } from 'sequelize';
import BaseModel from "./base.model";

class BookMarksModel extends BaseModel {
    static type = 'bookmark';
    
    public id?: number;
    public userID?: string;
    public commentID?: string;
    public createdAt?: string;
    public updatedAt?: string;
}

export default BookMarksModel

export const bookMarkAttributes= {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    userID: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: 'user_id'
    },
    commentID: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: 'comment_id'
    },
    createdAt: {
        type: DataTypes.DATE,
        default: Date.now(),
        field: 'created_at'
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
        field: 'updated_at'
    }
}

export const BookMarksSchema = (sequelize: Sequelize) => {
    BookMarksModel.init(bookMarkAttributes, {
        sequelize,
        tableName: 'BookMarks',
        timestamps: false,
        hooks: {
            beforeCreate: async (instance, options) => {
                instance.entityName = 'bookmark';
                instance.setDataValue('createdAt', Date.now());
                return Promise.resolve();
            },
            beforeUpdate: async (instance, options) => {
                instance.entityName = 'bookmark';
                instance.setDataValue('updatedAt', Date.now());
                return Promise.resolve();
            },
            beforeDestroy: async (instance, options) => {
                instance.entityName = 'bookmark';
                return Promise.resolve();
            }
        }
    });
    BookMarksModel.sync();
    return BookMarksModel;
}
