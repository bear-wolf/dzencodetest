import { Model, Sequelize, DataTypes } from 'sequelize';
import BaseModel from "./base.model";

class LikeModel extends BaseModel {
    static type: string = 'like';

    public id?: number;
    public userID?: string;
    public commentID?: string;
    public createdAt?: string;
    public updatedAt?: string;
}

export default LikeModel

export const likeAttributes= {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    userID: {
        type: DataTypes.BIGINT,
        allowNull: false,
        field: 'user_id'
    },
    commentID: {
        type: DataTypes.BIGINT,
        allowNull: false,
        field: 'comment_id'
    },
    createdAt: {
        type: DataTypes.DATE,
        default: Date.now(),
        field: 'created_at'
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
        field: 'updated_at'
    }
}

export const LikeSchema = (sequelize: Sequelize) => {
    LikeModel.init(likeAttributes, {
        sequelize,
        tableName: 'Likes',
        timestamps: false,
        hooks: {
            beforeCreate: async (instance, options) => {
                instance.entityName = 'like';
                instance.setDataValue('createdAt', Date.now());
                return Promise.resolve();
            },
            beforeUpdate: async (instance, options) => {
                instance.entityName = 'like';
                instance.setDataValue('updatedAt', Date.now());
                return Promise.resolve();
            },
            beforeDestroy: async (instance, options) => {
                instance.entityName = 'like';
                return Promise.resolve();
            }
        }
    });
    LikeModel.sync();
    return LikeModel;
}
