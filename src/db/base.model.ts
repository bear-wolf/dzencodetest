import {Model, Sequelize} from 'sequelize';
import 'dotenv/config';
import ElasticSearch from './../modules/elasticSearch'

export class BaseModel extends Model {
    public entityName: string | undefined;
}

export default BaseModel

export const BaseSchema = (sequelize: Sequelize) => {
    const saveDocument = async (instance: any) => {
        const json = instance.toJSON();

        await ElasticSearch.saveDocument(instance.entityName || '', json.id, json);
    }

    sequelize.addHook('afterCreate', saveDocument);
    sequelize.addHook('afterUpdate', saveDocument);
    sequelize.addHook('afterDestroy', async (instance: any) => {
        const json = instance.toJSON();

        await ElasticSearch.destroyDocument(instance.entityName || '', json.id);
    });

    return BaseModel;
}
