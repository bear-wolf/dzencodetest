import {DataTypes, Model, Sequelize} from 'sequelize';
import 'dotenv/config';
import {BaseModel} from "./base.model";

class UserModel extends BaseModel {
    static type: string = 'user';

    public id?: number;
    public firstName?: string;
    public lastName?: string;
    public username?: string;
    public email?: string;
    public password?: string;
    public isDelete?: boolean;
    public authorizationAt?: string;
    public createdAt?: string;
    public updatedAt?: string;

    toJSON() {
        const data = super.toJSON();

        delete data.password;
        return data;
    }
}

export default UserModel

export const userAttributes= {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    firstName: {
        type: DataTypes.STRING,
        allowNull: true,
        field: 'first_name'
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: true,
        field: 'last_name'
    },
    username: {
        type: DataTypes.STRING,
        allowNull: true
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    isDelete: {
        type: DataTypes.BOOLEAN,
        default: false,
        field: 'is_delete'
    },
    authorizationAt: {
        type: DataTypes.DATE,
        field: 'authorization_at'
    },
    createdAt: {
        type: DataTypes.DATE,
        default: Date.now(),
        field: 'created_at'
    },
    updatedAt: {
        type: DataTypes.DATE,
        field: 'updated_at'
    }
}

export const UserSchema = (sequelize: Sequelize) => {
    UserModel.init(userAttributes, {
        sequelize,
        tableName: 'Users',
        timestamps: false,
        defaultScope: {
            attributes: {
                exclude: ['password']
            }
        },
        hooks: {
            beforeCreate: async (instance, options) => {
                instance.entityName = 'user';
                return Promise.resolve();
            },
            beforeUpdate: async (instance, options) => {
                instance.entityName = 'user';
                instance.setDataValue('updatedAt', Date.now());
                return Promise.resolve();
            },
            beforeDestroy: async (instance, options) => {
                instance.entityName = 'user';
                return Promise.resolve();
            }
        }
    });

    UserModel.sync();
    return UserModel;
}
