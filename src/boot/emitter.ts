import EventEmitter from 'node:events';
import {Log} from "../db";
import nodeCache from "../boot/cache";

const emitterObject: any = {
    emitter: new EventEmitter(),
    connections: [],
    emit: function (ev: any, data: any) {
        this.emitter.emit(ev, data)
    },
    addConnection: function (res: any) {
        if (this.connections.some((resE: any) => resE.req.userdata && resE.req.userdata.id === res.req.userdata.id)) {
            this.connections.splice(
                this.connections.findIndex(
                    (resE: any) => resE.req.userdata.id === res.req.userdata.id
                ),
                1,
                res
            );
        } else this.connections.push(res);

        const conTimeout = 60 * 60 * 1000;
        setTimeout(() => {
            this.connections.splice(this.connections.findIndex((resE: any) => resE.req.userdata.id === res.req.userdata.id), 1);
        }, conTimeout);
    },
    deleteConnection: function (req: any) {
        if (this.connections.some((resE: any) => resE.req.userdata.id === req.userdata.id)) {
            this.connections.splice(
                this.connections.findIndex((resE: any) => resE.req.userdata.id === req.userdata.id),
                1
            );
        }
    }
};

emitterObject.getOuterData = () => emitterObject.emitter.on('outer_api_stream', (toStream: any) => {
    emitterObject.connections.map((resE: any) => {
        const data = {message: 'Its json from outer API'};
        resE.json({
            data
            //toStream
        })
    });
})

emitterObject.emitter.on('save_req', async (data: any) => {
    const {req, isUpdate} = data;

    if (isUpdate) {
        let cacheLogData: any = nodeCache.get(req.uniqueID)

        if (cacheLogData) {
            cacheLogData = JSON.parse(cacheLogData)
            const log: any = await Log.findByPk(cacheLogData.id);

            log.author = req.userdata;
            await log.save();
        }
        return
    }

    const log = await Log.create({
        author: req.userdata,
        method: req.method,
        url: req.url,
        req: JSON.stringify(req.body)
    }, {
        raw: true
    })
    nodeCache.set(req.uniqueID, JSON.stringify(log), 60000); // 1m
})

export default emitterObject;