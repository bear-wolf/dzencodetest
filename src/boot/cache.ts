import NodeCache from "node-cache";

const nodeCache = new NodeCache({
    stdTTL: 1000
});

export default nodeCache