import 'dotenv/config'
import EventEmitter from 'node:events';

it("Node events", async () => {
    const eventEmitter = new EventEmitter();

    /*
    *   once(): add a one-time listener
        removeListener() / off(): remove an event listener from an event
        removeAllListeners(): remove all listeners for an event
    * */
    eventEmitter.on('start', (start, end) => {
        console.log(`started from ${start} to ${end}`);
    });
    eventEmitter.emit('start', 1, 100);

    expect(true).toBe(true)
});