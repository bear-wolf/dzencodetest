import 'dotenv/config'
import BcryptHelper from "../src/api/v1/helpers/bcrypt.helper";

it("Bcrypt hash", async () => {
    const v1 = await BcryptHelper.hashPassword('14714711')
    const v2 = await BcryptHelper.hashPassword('14714711')
    console.log('Hash 1', v1)
    console.log('Hash 2', v2)
    expect(v1).toBe(v1)
});