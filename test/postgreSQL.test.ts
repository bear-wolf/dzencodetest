import {DataTypes, QueryTypes, Sequelize} from 'sequelize';
import 'dotenv/config'

const {
    PGSQL_DB_HOST, PGSQL_DB_PORT, PGSQL_DB_NAME, PGSQL_DB_USERNAME, PGSQL_DB_PASSWORD
} = process.env;


describe("Test PostgresSQL database", () => {
    const sequelize = new Sequelize(
        `postgres://${PGSQL_DB_USERNAME}:${PGSQL_DB_PASSWORD}@${PGSQL_DB_HOST}:${PGSQL_DB_PORT}/${PGSQL_DB_NAME}`, {
            host: PGSQL_DB_HOST,
            dialect: 'postgres',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            },
            logging: false
        }
    );
    const queryInterface = sequelize.getQueryInterface();

    it("Create temporary table", async () => {
        let status;
        try {
            const createSyntax = 'CREATE TEMPORARY TABLE TemporaryTable(first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL)';
            const [list, Result] = await sequelize.query(createSyntax, {
                raw: true
            });
            // console.log('Create temporary table:', Result)
            status = true;
        } catch (err: any) {
            console.log('ERROR', err)
            status = false;
        }

        expect(status).toEqual(true);
    });

    it("Create table", async () => {
        let status;
        try {
            await queryInterface.createTable('Temp', {
                name: DataTypes.STRING,
                isTest: DataTypes.BOOLEAN
            });
            status = true;
        } catch (err: any) {
            status = false;
        }

        expect(status).toEqual(true);
    });
});